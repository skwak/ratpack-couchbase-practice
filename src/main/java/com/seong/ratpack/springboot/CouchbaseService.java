package com.seong.ratpack.springboot;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.bucket.BucketType;
import com.couchbase.client.java.cluster.BucketSettings;
import com.couchbase.client.java.cluster.ClusterManager;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by sk on 12/06/2016.
 */

@Service
public class CouchbaseService {
    Logger logger = LogManager.getLogger(CouchbaseService.class);

    private Cluster cluster;
    private Bucket bucket;
    public final String bucketPassword;
    public final String cbUrl;
    public final boolean dataNeedsToBeImported;

    public Cluster getCluster() {
        return cluster;
    }

    public Bucket getBucket() {
        return bucket;
    }

    @Autowired
    CouchbaseService(@Value("${couchbase.seed.nodes}") String seedNodes, @Value("${couchbase.bucket.name}") String bucketName, @Value("${couchbase.bucket.pw}") String bucketPassword) throws JsonProcessingException {
        this.bucketPassword = bucketPassword;
        this.cbUrl = seedNodes;
        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder().connectTimeout(10000).build();
        this.cluster = CouchbaseCluster.create(env, seedNodes);
        dataNeedsToBeImported = createBucketIfnotPresent(cluster, seedNodes, bucketName, bucketPassword);
        this.bucket = cluster.openBucket(bucketName, bucketPassword);
        this.logger.info("Couchbase bucket: " + this.bucket.name());
    }

    /**
     * @param cluster
     * @param seedNodes
     * @param bucketName
     * @param bucketPassword
     * @return return false if the bucket is already present, return true if the bucket is not already present and created one.
     * @throws JsonProcessingException
     */
    private boolean createBucketIfnotPresent(Cluster cluster, String seedNodes, String bucketName, String bucketPassword) throws JsonProcessingException {
        ClusterManager cManager = cluster.clusterManager("Administrator", "Administrator");
        BucketSettings myBucketSetting = createMyBucketSetting(cluster, seedNodes, bucketName, bucketPassword);

        if (!cManager.hasBucket(bucketName)) {
            cManager.insertBucket(myBucketSetting);
            return true;
        }
        return false;
    }

    private BucketSettings createMyBucketSetting(Cluster cluster, String seedNodes, String bucketName, String bucketPassword) {
        return new BucketSettings() {
            @Override
            public String name() {
                return bucketName;
            }

            @Override
            public BucketType type() {
                return BucketType.COUCHBASE;
            }

            @Override
            public int quota() {
                return 200;
            }

            @Override
            public int port() {
                return 0;
            }

            @Override
            public String password() {
                return bucketPassword;
            }

            @Override
            public int replicas() {
                return 0;
            }

            @Override
            public boolean indexReplicas() {
                return false;
            }

            @Override
            public boolean enableFlush() {
                return true;
            }
        };
    }


}
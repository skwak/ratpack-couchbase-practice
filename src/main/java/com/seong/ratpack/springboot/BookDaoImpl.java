package com.seong.ratpack.springboot;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.document.RawJsonDocument;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


/**
 * Created by sk on 14/06/2016.
 */

@Service
public class BookDaoImpl implements BookDao {

    Logger logger = LogManager.getLogger(BookDaoImpl.class);

    private CouchbaseClientService couchbaseClientService;
    private CouchbaseService couchbaseService;
    private Cluster cluster;
    private Bucket bucket;

    @Autowired
    public BookDaoImpl(CouchbaseService couchbaseService, CouchbaseClientService couchbaseClientService) {
        this.couchbaseService = couchbaseService;
        this.couchbaseClientService = couchbaseClientService;
        this.cluster = couchbaseService.getCluster();
        this.bucket = couchbaseService.getBucket();
        importDataBase();

    }

    @Override
    public Observable<RawJsonDocument> insert(Book entity, String key) {
        JSONSerializer serializer = new JSONSerializer();
        String jsonString = serializer.deepSerialize(entity);
        RawJsonDocument jsonDoc = RawJsonDocument.create(key, jsonString);
        Observable<RawJsonDocument> retJsonDoc = bucket.async().insert(jsonDoc);
        return retJsonDoc;
    }

    @Override
    public Observable<RawJsonDocument> replace(Book entity) {
        return null;
    }

    @Override
    public Observable<RawJsonDocument> remove(Book entity) {
        return null;
    }

    @Override
    public Observable<RawJsonDocument> get(Book entity) {
        return null;
    }

    @Override
    public Observable<RawJsonDocument> get(String bookId) {
        Observable<RawJsonDocument> retJsonDoc = bucket.async().get(bookId, RawJsonDocument.class);
        return retJsonDoc;
    }

    @Override
    public Observable<List<RawJsonDocument>> getEveryBook() throws IOException, ExecutionException, InterruptedException {
        List<String> list = new ArrayList<>();
        couchbaseClientService.getViewResponse().forEach(a->list.add(a.getId()));

        String[] array = list.toArray(new String[0]);

        return Observable.from(array)
                .flatMap(id -> {
                    return bucket
                            .async()
                            .get(id,RawJsonDocument.class);
                })
                .toList();
    }

    @Override
    public Observable<RawJsonDocument> exists(String bookId) {
        return null;
    }

    @Override
    public Book deserialiser(RawJsonDocument rjDoc) {
        return new JSONDeserializer<Book>().deserialize(rjDoc.content(), Book.class);
    }


    public void importDataBase() {
        if (couchbaseService.dataNeedsToBeImported) {
            Map<String, Book> storage = new HashMap<String, Book>();
            Book book1 = new Book();
            book1.setBookName("what man lives by");
            book1.setAuthor("Leo Tolstoy");
            storage.put("book1", book1);

            Book book2 = new Book();
            book2.setBookName("Robinson Crusoe");
            book2.setAuthor("Daniel Defoe");
            storage.put("book2", book2);

            for (String bookId : storage.keySet()) {
                insert(storage.get(bookId), bookId);
            }
        }
    }
}
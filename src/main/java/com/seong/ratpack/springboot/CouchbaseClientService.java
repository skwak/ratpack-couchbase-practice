package com.seong.ratpack.springboot;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.DesignDocument;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.ViewDesign;
import com.couchbase.client.protocol.views.ViewResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sk on 14/06/2016.
 */
@Service
public class CouchbaseClientService {

    Logger logger = LogManager.getLogger(CouchbaseClientService.class);
    static CouchbaseClient cClient = null;

    @Autowired
    CouchbaseClientService(@Value("${couchbase.seed.nodes}") String seedNodes, @Value("${couchbase.bucket.name}") String bucketName, @Value("${couchbase.bucket.pw}") String bucketPassword) throws JsonProcessingException {
        connect(seedNodes, bucketName, bucketPassword);
        createView();
    }

    public ViewResponse getViewResponse() {
        com.couchbase.client.protocol.views.View view = cClient.getView("newdesign", "something");
        Query query = new Query();
        return cClient.query(view, query);
    }

    private void connect(String seedNodes, String bucketName, String bucketPassword) {
        List<URI> uris = new LinkedList<URI>();
        uris.add(URI.create("http://" + seedNodes + ":8091/pools"));
        System.setProperty("viewmode", "development");
        try {
            cClient = new CouchbaseClient(uris, bucketName, bucketPassword);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createView() {
        boolean somethingViewIsNotThere;
        try{
            cClient.getView("newdesign", "something");
            somethingViewIsNotThere =false;
        }
        catch (Exception e){
            somethingViewIsNotThere=true;
        }

        if (somethingViewIsNotThere) {
            DesignDocument designDoc = new DesignDocument("newdesign");
            String viewName = "something";
            String mapFunction = "function (doc, meta) { \n" +
                    "if(meta.type == \"json\") {  \n" +
                    "  emit(doc);   \n" +
                    " } \n" +
                    "}";
            ViewDesign viewDesign = new ViewDesign(viewName, mapFunction);
            designDoc.getViews().add(viewDesign);
            cClient.createDesignDoc(designDoc);
        }
    }

}

package com.seong.ratpack.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ratpack.func.Action;
import ratpack.registry.Registry;
import ratpack.registry.RegistrySpec;
import ratpack.server.RatpackServerSpec;
import ratpack.spring.Spring;

@Configuration
public class BeanConfigurator {

//	@Autowired
//	AppSpringBootConfig appSpringBootConfig;

	@Autowired
	BookDaoImpl bookDaoImpl;

	@Bean
	Action<RegistrySpec> registrySpec() {
		return r -> r.add(BookDao.class, bookDaoImpl);
	}

//	@Bean
//	Action<RatpackServerSpec> registy(){
//		return s -> s.registry(Spring.spring(AppSpringBootConfig.class));
//	}
}

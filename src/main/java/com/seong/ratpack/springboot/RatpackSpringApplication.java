package com.seong.ratpack.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ratpack.rx.RxRatpack;
import ratpack.spring.config.EnableRatpack;

@SpringBootApplication
@EnableRatpack
public class RatpackSpringApplication {

	public static void main(String[] args) {
		RxRatpack.initialize();
		SpringApplication.run(RatpackSpringApplication.class, args);
	}
}

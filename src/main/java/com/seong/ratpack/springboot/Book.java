package com.seong.ratpack.springboot;

import java.io.Serializable;

public class Book implements Serializable{
  private String bookName;
  private String author;

  public String getBookName() {
    return bookName;
  }
  public String getAuthor() {
    return author;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
}

package com.seong.ratpack.springboot;

import com.couchbase.client.java.document.RawJsonDocument;
import rx.Observable;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by sk on 14/06/2016.
 */
public interface BookDao {
    public Observable<RawJsonDocument> insert(Book entity, String key);
    public Observable<RawJsonDocument> replace(Book entity);
    public Observable<RawJsonDocument> remove(Book entity);
    public Observable<RawJsonDocument> get(Book entity);
    public Observable<RawJsonDocument> get(String bookId);
    public Observable<List<RawJsonDocument>> getEveryBook() throws IOException, ExecutionException, InterruptedException;
    public Observable<RawJsonDocument> exists(String bookId);

    public Book deserialiser(RawJsonDocument rjDoc);
}

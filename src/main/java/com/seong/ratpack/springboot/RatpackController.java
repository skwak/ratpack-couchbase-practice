package com.seong.ratpack.springboot;

import com.couchbase.client.java.document.RawJsonDocument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.http.client.HttpClient;
import ratpack.http.client.ReceivedResponse;
import ratpack.jackson.Jackson;
import ratpack.rx.RxRatpack;
import rx.Observable;

import java.net.URI;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ratpack.jackson.Jackson.json;

@Controller
@RequestMapping("/")
public class RatpackController {

    Logger logger = LogManager.getLogger(RatpackController.class);

//    private final BookDaoImpl bookDaoImpl;
//    @Autowired
//    RatpackController(BookDaoImpl bookDaoImpl) {
//        this.bookDaoImpl = bookDaoImpl;
//    }
//
//	@RequestMapping(method = RequestMethod.GET)
//	public Book get(@PathVariable String bookId, HttpServletResponse response) {
//		 Observable<Book> book = bookDaoImpl.getBook(bookId);
//		if(book!= null)return book;
//		response.setStatus(404);
//		return null;
//	}
//
//	@RequestMapping(method=RequestMethod.GET)
//	List<Book> list() {
//	return this.bookDaoImpl.getEveryBooksList();
//	}

	@RequestMapping("/")
	@ResponseBody String bootRoot() {
		return "This is Spring Boot";
	}

    @RequestMapping("/ratpack")
    @ResponseBody
    Map bootRatpack() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("http://localhost:5050/json", Map.class);
    }

    @RequestMapping("/sendbook_to_ratpack")
    @ResponseBody
    List senduser() {
        RestTemplate restTemplate = new RestTemplate();
        logger.info("**************** sent");
        List responseList = restTemplate.getForObject("http://localhost:5050/book", List.class);
        logger.info(responseList.stream().map(Object::toString).collect(Collectors.joining("      ,     ")));
        return responseList;
    }

    @Bean
    Action<Chain> chain() {
        return chain -> chain
                .prefix("book", pchain -> pchain
                        .get(":bookid", ctx -> {
                            BookDaoImpl bookDaoImpl = ctx.get(BookDaoImpl.class);
                            String bookId = ctx.getPathTokens().get("bookid");
                            Observable<RawJsonDocument> bookObs = bookDaoImpl.get(bookId);
                            RxRatpack.promiseSingle(bookObs).then(book ->
                                    ctx.render(json(bookDaoImpl.deserialiser(book)))
                            );
                        })
                        .get(ctx -> {
                            BookDaoImpl bookDaoImpl = ctx.get(BookDaoImpl.class);
                            Observable<List<RawJsonDocument>> bookObs = bookDaoImpl.getEveryBook();
                            RxRatpack.promiseSingle(bookObs).then(books -> {
                                books.stream().forEach(ele -> logger.info(ele + " :   ******************* "));
                                List<Book> bookList = books.stream()
                                        .map(book -> bookDaoImpl.deserialiser(book))
                                        .collect(Collectors.toList());
                                ctx.render(json(bookList));
                            });
                        })
                )
                .get(ctx -> ctx.render("Hello from Ratpack in Spring Boot!"))
                .get("json", ctx -> {
                    Map<String, String> map = new HashMap<>();
                    map.put("date", Instant.now().toString());
                    ctx.render(Jackson.json(map));
                })
                .get("boot", ctx -> {
                    HttpClient client = ctx.get(HttpClient.class);
                    client.get(new URI("http://localhost:8080"))
                            .map(ReceivedResponse::getBody)
                            .map(body -> "Received from Spring Boot: " + body.getText())
                            .then(ctx::render);
                });
    }
}
